#include <iostream>
#include <fstream>
#include  <string>

int table_row = 0;

double sum_table(std::ifstream &file)
{
	double sum = 0.0;
	while(file.good())
	{
		std::string line;
		int pos = 0, old_pos;
		std::getline(file, line);
		for(int i = 0; i < table_row && pos != std::string::npos; i++)
		{
			old_pos = pos;
			pos = line.find('\t', pos+1);
		}
		if(pos != std::string::npos)
		{
			std::string number(line, old_pos, old_pos - pos);
			char * ret;
			sum += std::strtod(number.c_str(), &ret);
			if(ret != NULL && (*ret == '.' || *ret == ','))
				std::cerr << "Error" << std::endl;
		}
		else
		{
			std::string number(line, old_pos);
			char * ret;
			sum += std::strtod(number.c_str(), &ret);
			if(ret != NULL &&(*ret == '.' || *ret == ','))
				std::cerr << "Error" << std::endl;
		}
	}
	return sum;
}

int main(int argc, char *argv[])
{
	if(argc > 1)
	{
		char *ret;
		table_row = std::strtol(argv[1], &ret, 10);
		if(table_row < 1)
		{
			abort();
		}
	}
	for(int i = 2; i < argc; i++)
	{
		std::ifstream file(argv[i]);
		double sum = sum_table(file);
		std::cout << argv[i] << ": " << sum << std::endl;
	}
}
