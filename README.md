# tab-table-sum

This program sums up the numbers in a column of tab-separated CSV-files.

Run with
```
./tab-table-sum column file1 [file2] [file3] ...
```

Column-numbers start with 0.
